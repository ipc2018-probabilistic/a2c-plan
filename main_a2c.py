import argparse
import os
import torch
import torch.nn as nn
import torch.nn.functional as F
from tools import ensure_directory_exists, weights_init, normalized_columns_initializer
from a2c import ActorCritic
from RDDLEnv import RDDLEnv
import numpy.ctypeslib as npct
import numpy as np
import pwd
import ctypes


class A2CNet(nn.Module):
    def __init__(self, input_size, actions):
        super(A2CNet, self).__init__()
        self.fc0 = nn.Linear(input_size, input_size*3)
        self.fc1 = nn.Linear(input_size*3, input_size*2)
        self.actor_linear = nn.Linear(input_size*2, actions)
        self.critic_linear = nn.Linear(input_size*2, 1)

        self.apply(weights_init)
        self.actor_linear.weight.data = normalized_columns_initializer(self.actor_linear.weight.data, 0.01)
        self.actor_linear.bias.data.fill_(0)
        self.critic_linear.weight.data.fill_(0)
        self.critic_linear.bias.data.fill_(0)

    def forward(self, input):
        x = F.relu(self.fc0(input))
        x = F.relu(self.fc1(x))
        return self.actor_linear(x), self.critic_linear(x)


# Murugeswari
global ac_net, ac_algo, _env

def cbtest(state, probs):
    global ac_net, ac_algo, _env
    s = npct.as_array(state, (_env.num_state_vars,))
    ac_net.eval()
    a, p = ac_algo.test2(ac_net, s)
    for i in range(_env.num_enum_actions) :
        probs[i] = p[i]
    return a


def cbtrain() :
    global ac_net, ac_algo, _env
    ac_net = A2CNet(_env.num_state_vars, _env.num_enum_actions)
    homedir = pwd.getpwuid(os.getuid()).pw_dir
    net_path = os.path.join(homedir, args.env + '_model.p')
    plots_dir = ensure_directory_exists(os.path.join(homedir, args.env + '_plots'))
    if args.cuda:
        ac_net = ac_net.cuda()
    if os.path.exists(net_path) and not args.scratch:
        ac_net.load_state_dict(torch.load(net_path))
    ac_algo = ActorCritic(args.gamma, args.tau, args.critic_loss_coef, args.entropy_coef, recurrent=None)
    ac_net.train()
    ac_net = ac_algo.train(ac_net, _env, net_path, plots_dir, args)
    ac_net.load_state_dict(torch.load(net_path))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Actor Critic')
    parser.add_argument('--gamma', type=float, default=0.99, metavar='G', help='discount factor (default: 0.99)')
    parser.add_argument('--tau', type=float, default=1.00, metavar='T', help='parameter for GAE (default: 1.00)')
    parser.add_argument('--entropy-coef', type=float, default=0.01, help='entropy term coefficient (default: 0.01)')
    parser.add_argument('--critic-loss-coef', type=float, default=1, help='value loss coefficient (default: 0.5)')
    parser.add_argument('--seed', type=int, default=10, metavar='N', help='random seed (default: 10)')
    parser.add_argument('--render', action='store_true', default=False, help='render the environment')
    parser.add_argument('--hybrid', action='store_true', default=False, help='use hybrid reward')
    parser.add_argument('--batch_size', type=int, default=10, help='Batch Size(No. of Episodes) for Training')
    parser.add_argument('--beta', type=float, default=0.01, help='Rate for Entropy')
    parser.add_argument('--log-interval', type=int, default=5, help='interval between training status logs (default: 5)')
    parser.add_argument('--no_cuda', action='store_true', default=True, help='Disables Cuda Usage')
    parser.add_argument('--train', action='store_true', default=True, help='Train the network')
    parser.add_argument('--test', action='store_true', default=True, help='Test the network')
    parser.add_argument('--train_episodes', type=int, default=1000, help='Episode count for training')
    parser.add_argument('--test_episodes', type=int, default=100, help='Episode count for testing')
    parser.add_argument('--lr', type=float, default=0.01, help='Learning Rate for Training (Adam Optimizer)')
    parser.add_argument('--scratch', action='store_true', default=False, help='Train the network from scratch ( or Does not load pre-trained model)')
    parser.add_argument('--env', default=None, metavar='ENV', help='environment to train')
    parser.add_argument('--rddl_base_dir', default='./simulator')
    parser.add_argument('--host', default='localhost')
    parser.add_argument('--port', default=2323)

    args = parser.parse_args()
    args.cuda = torch.cuda.is_available() and (not args.no_cuda)
    vis = None

    _env = RDDLEnv(args.env)
    _env.connectToServer(args.host, args.port, cbtrain, cbtest)
