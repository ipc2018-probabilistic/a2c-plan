import random
import time
import numpy as np
import torch
import torch.nn as nn
from torch.optim import Adam
import torch.nn.functional as F
from torch.autograd import Variable
from torch.distributions import Categorical
import os, psutil

class ActorCritic:
    """
    Actor Critic Implementation
    """

    def __init__(self, gamma, tau, critic_loss_coef, entropy_coef, max_grad_norm=None, recurrent=None):
        self.recurrent = recurrent
        self.gamma = gamma
        self.tau = tau
        self.entropy_coef = entropy_coef
        self.critic_loss_coef = critic_loss_coef
        self.max_grad_norm = max_grad_norm


    def train(self, net, env, net_path, plots_dir, args):
        memory_limit = 3584000000 #3.5 GB
        process = psutil.Process(os.getpid())
        training_time = self.testrun(net, env)
        optimizer = Adam(net.parameters(), lr=args.lr)
        mse_loss = nn.MSELoss.cuda() if args.cuda else nn.MSELoss()
        test_perf_data = []
        train_perf_data = []
        best = None
        n_trajectory_info = []
        st = time.time(); time_exceeded = False; episode = 0; memory_exceeded = False
        while not time_exceeded and not memory_exceeded:
            memory_usage = process.memory_info().rss
            #print(memory_usage)
            if memory_usage > memory_limit:
                print('Memory limit exceeded')
                memory_exceeded = True
                break
            if (time.time() - st) > training_time:
                print('Training time limit exceeded')
                time_exceeded = True
                break
            episode += 1
            net.train()
            done = False
            total_reward = 0
            log_probs = []
            ep_rewards = []
            critic_info = []
            ep_obs = []
            obs = env.reset()
            entropies = []
            while not done:
                ep_obs.append(obs)
                obs = Variable(torch.FloatTensor(obs.tolist())).unsqueeze(0)

                logit, critic = net(obs)
                prob = F.softmax(logit,dim=1)
                log_prob = F.log_softmax(logit,dim=1)

                action = prob.multinomial(num_samples=1).data
                action = int(action[0])
                obs, reward, done, info = env.doAction(action, prob.data.numpy()[0])
                lprob = log_prob[0][action]
                entropy = -(log_prob * prob).sum(1)

                log_probs.append(lprob)
                ep_rewards.append(reward / (env.max_reward - env.min_reward))
                critic_info.append(critic)
                entropies.append(entropy)
                total_reward += reward

            train_perf_data.append(total_reward)
            n_trajectory_info.append((ep_obs, ep_rewards, critic_info, log_probs, entropies))

            # Update the network after collecting n trajectories
            if episode % args.batch_size == 0:
                batch_policy_loss, batch_critic_loss = 0, 0
                for trajectory_info in n_trajectory_info:
                    policy_loss, critic_loss = 0, 0
                    obs, _rewards, _critic, _log_probs, _entropies = trajectory_info
                    R = Variable(torch.zeros(1, 1))
                    gae = torch.zeros(1, 1)
                    _critic.append(Variable(torch.zeros(1, 1)))
                    for i in reversed(range(len(_rewards))):
                        R = self.gamma * R + _rewards[i]
                        advantage = R - _critic[i]
                        critic_loss = critic_loss + 0.5 * advantage.pow(2)
                        delta_t = _rewards[i] + self.gamma * _critic[i + 1].data - _critic[i].data
                        gae = gae * self.gamma * self.tau + delta_t
                        policy_loss = policy_loss - _log_probs[i] * Variable(gae) - self.entropy_coef * _entropies[i]
                    batch_policy_loss += policy_loss
                    batch_critic_loss += critic_loss
                batch_policy_loss /= args.batch_size
                batch_critic_loss /= args.batch_size
                optimizer.zero_grad()
                loss = (batch_policy_loss + self.critic_loss_coef * batch_critic_loss)
                loss.backward()
                if self.max_grad_norm is not None:
                    torch.nn.utils.clip_grad_norm(net.parameters(), self.max_grad_norm)
                optimizer.step()
                n_trajectory_info = []
            # print('Train=> Episode:{} Reward:{} Length:{}'.format(episode, total_reward, len(ep_rewards)))

            # test and log
            if episode % 20 == 0:
                test_reward = self.test(net, env, 10, log=True)
                test_perf_data.append(test_reward)
                print('Test:', test_reward)
                if best is None or best <= test_reward:
                    torch.save(net.state_dict(), net_path)
                    best = test_reward
                    print('Model Saved!')
        return net


    def test(self, net, env, episodes, log=False, render=False, sleep=0):
        net.eval()
        all_episode_rewards = 0
        for episode in range(episodes):
            done = False
            episode_reward = 0
            obs = env.reset()
            ep_actions = []  # just to exit early if the agent is stuck
            steps = 0
            while not done:
                obs = Variable(torch.FloatTensor(obs.tolist())).unsqueeze(0)
                logit, critic = net(obs)
                prob = F.softmax(logit, dim=1)
                action = int(prob.max(1)[1].data.cpu().numpy()[0])
                obs, reward, done, info = env.doAction(action, prob.data.numpy()[0])
                episode_reward += reward
                steps += 1
            all_episode_rewards += episode_reward
        return all_episode_rewards / episodes


    #Murugeswari
    def test2(self, net, state):
        net.eval()
        obs = state
        obs = Variable(torch.FloatTensor(obs.tolist())).unsqueeze(0)
        logits, critic = net(obs)
        prob = F.softmax(logits, dim=1)
        action = int(prob.max(1)[1].data.cpu().numpy()[0])
        return action, prob.data.numpy()[0]


    def testrun(self, net, env) :
        EPISODES = int(env.num_rounds / 5)
        net.eval()
        st = time.time()
        for i in range(EPISODES) :
            done = False
            obs = env.reset()
            while not done :
                obs = Variable(torch.FloatTensor(obs.tolist())).unsqueeze(0)
                logits, _ = net(obs)
                prob = F.softmax(logits, dim=1)
                action = int(prob.max(1)[1].data.cpu().numpy()[0])
                obs, reward, done, info = env.doAction(action, prob.data.numpy()[0])
        en = time.time()
        t = (en - st)
        eval_time = t * int(env.num_rounds * 2)
        training_time = (((env.remaining_time / 1000) - eval_time)) * 0.75
        print('Evaluation: {}  Training: {} '.format(eval_time, training_time))
        return training_time
