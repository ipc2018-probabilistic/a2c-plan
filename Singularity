Bootstrap: docker
From: ubuntu:bionic

%setup
     ## The "%setup"-part of this script is called to bootstrap an empty
     ## container. It copies the source files from the branch of your
     ## repository where this file is located into the container to the
     ## directory "/planner". Do not change this part unless you know
     ## what you are doing and you are certain that you have to do so.

    REPO_ROOT=`dirname $SINGULARITY_BUILDDEF`
    cp -r $REPO_ROOT/ $SINGULARITY_ROOTFS/planner

%post

    ## The "%post"-part of this script is called after the container has
    ## been created with the "%setup"-part above and runs "inside the
    ## container". Most importantly, it is used to install dependencies
    ## and build the planner. Add all commands that have to be executed
    ## once before the planner runs in this part of the script.


    ## Install all necessary dependencies.
    export DEBIAN_FRONTEND=noninteractive
    apt-get update
    apt-get -y install mercurial g++ make bison flex libbdd-dev

    ## install python dependencies
    ## apt-get -y install python-pip python-dev python3-pip python3-dev build-essential
    apt-get -y install python3-pip python3-dev build-essential
    ## pip3 install --upgrade pip

    pip3 install -r /planner/requirements.txt
    pip3 install http://download.pytorch.org/whl/cpu/torch-0.3.1-cp36-cp36m-linux_x86_64.whl

    cd /planner
    cd src/rddl_parser
    make clean
    make
    cp rddl-parser ../..
    cd ../search
    make clean
    make
    cd .obj
    g++ -shared -o clibxx.so -fPIC *.o utils/* -lbdd -lstdc++fs
    cp clibxx.so ../../..
    cd ../../..

%runscript
    ## The runscript is called whenever the container is used to solve
    ## an instance.

    INSTANCE=$1
    PORT=$2

    ## Call your planner
    pwd
    cd /planner
    python3 main_a2c.py --host 127.0.0.1 --port $PORT --env $INSTANCE


## Update the following fields with meta data about your submission.
## Please use the same field names and use only one line for each value.
%labels
Name                  A2C-Plan
Description           Model Free RL to learn a policy
Authors               Anurag Koul <koula@oregonstate.edu>
SupportsIntermFluents no
SupportsEnums         yes
